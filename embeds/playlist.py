from typing import List

import discord.embeds
from player.song import Song


def new_playlist_embed(playlist: List[Song]) -> discord.Embed:
	playlist_embed = discord.embeds.Embed()
	playlist_embed.title = "Queued songs"
	playlist_description = ""

	for index, song in enumerate(playlist):
		playlist_description += "{}. {} {}\n".format(index + 1, "(Now playing)" if index == 0 else "", song.details["title"])

	playlist_embed.description = playlist_description

	return playlist_embed
