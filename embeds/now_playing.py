import discord.embeds
from player.song import Song


def new_now_playing_embed(song: Song) -> discord.Embed:
	now_playing_embed = discord.Embed()
	now_playing_embed.title = "Now playing!"
	now_playing_embed.set_image(url=song.details["thumbnail_url"])
	now_playing_embed.add_field(name="Title:  ", value=song.details["title"], inline=False)
	length = song.details["length"]
	length_str = "{}:{:02d}".format(int(length / 60), length % 60)
	now_playing_embed.add_field(name="Length: ", value=length_str)
	now_playing_embed.add_field(name="Channel:", value=song.details["author"])

	return now_playing_embed
