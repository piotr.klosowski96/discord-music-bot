from typing import *
from discord import FFmpegOpusAudio, VoiceClient
from enum import Enum
from pytube import YouTube
from player.song import Song


class PlayerState(Enum):
	PLAYING = 1
	STOPPED = 2
	PAUSED = 3
	NOT_INITIALIZED = 4


class MusicPlayer:
	def __init__(self, ffmpeg_path):
		self.voice_client: Optional[VoiceClient] = None
		self.queue: List[Song] = []
		self.state: PlayerState = PlayerState.NOT_INITIALIZED
		self.audio_source: Optional[FFmpegOpusAudio] = None
		self.youtube_stream: Optional[YouTube] = None
		self.ffmpeg_path: AnyStr = ffmpeg_path
		self.current_song: Optional[Song] = None

	def set_voice_channel(self, voice_client: VoiceClient):
		self.voice_client = voice_client

	def _play_next_after_this_song_finished(self):
		self.skip()
		self.play()

	def play(self):
		if self.voice_client is None:
			return

		if len(self.queue) == 0:
			return

		self.current_song = self.queue[0]
		self.audio_source = FFmpegOpusAudio(executable=self.ffmpeg_path, source=self.current_song.filename)
		self.voice_client.play(self.audio_source, after=lambda e: self._play_next_after_this_song_finished())

	def pause(self):
		if self.voice_client is not None:
			self.voice_client.pause()

	def stop(self):
		if self.voice_client is not None:
			self.voice_client.stop()

	def skip(self):
		self.current_song = None
		self.queue = self.queue[1:]

		if len(self.queue) == 0:
			return

	def is_playing(self):
		self.voice_client.is_playing()

	def enqueue(self, url: AnyStr):
		yt = YouTube(url)
		path = yt.streams.filter(only_audio=True).order_by("abr").asc().first().download()
		song = Song(path, title=yt.title, author=yt.author, description=yt.description, thumbnail_url=yt.thumbnail_url,
		            length=yt.length)
		self.queue.append(song)

	def playlist(self) -> List[Song]:
		return self.queue

	def now_playing(self) -> Optional[Song]:
		return self.current_song
