import typing


class Song:
	def __init__(self, filename: typing.AnyStr, **details):
		self.filename: typing.AnyStr = filename
		self.details = dict(details)
