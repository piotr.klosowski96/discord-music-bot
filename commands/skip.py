from commands.command import Command
from embeds.now_playing import new_now_playing_embed


class Skip(Command):
	async def run(self, ctx):
		music_player = ctx["music_player"]
		command_args = ctx["command_args"]
		message = ctx["message"]
		skip_count = 1

		if music_player is None:
			print("No music player instance found!")
			return

		if len(command_args) != 0:
			skip_count = int(command_args[0])

		music_player.stop()
		for i in range(skip_count):
			music_player.skip()
		music_player.play()

		song = music_player.now_playing()
		if song is not None:
			await message.channel.send(embed=new_now_playing_embed(song))
