from commands.command import Command


class Summon(Command):
	async def run(self, ctx):
		message = ctx["message"]
		music_player = ctx["music_player"]

		if message.author.voice is None:
			await message.channel.send("You need to join a voice channel in order to use this bot.")
			return

		voice_channel = await message.author.voice.channel.connect()

		if music_player is not None:
			music_player.set_voice_channel(voice_channel)
