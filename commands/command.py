from abc import abstractmethod, ABC


class Command(ABC):
	@abstractmethod
	def run(self, ctx):
		pass
