from discord import FFmpegOpusAudio
from commands.command import Command
from pytube import YouTube

class Search(Command):
	async def run(self, message, voice_client):
		command = str(message.content).split()
		if len(command) != 2:
			print("dupa")

		file_path = YouTube(command[-1]).streams.filter(only_audio=True).order_by("abr").asc().first().download()

		print(file_path)
		voice_client.play(FFmpegOpusAudio(executable="D:\\ffmpeg\\ffmpeg.exe", source=file_path))