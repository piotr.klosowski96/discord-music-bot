import discord

from commands.command import Command


class Queue(Command):
	async def run(self, ctx):
		music_player = ctx["music_player"]
		message = ctx["message"]
		url = ctx["command_args"][0]

		if music_player is None:
			print("No music player instance found!")
			return

		music_player.enqueue(url)
		last_queued = music_player.playlist()[-1]

		await message.channel.send("Successfully queued: {}".format(last_queued.details["title"]))
