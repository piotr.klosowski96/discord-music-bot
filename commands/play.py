from commands.command import Command
from embeds.now_playing import new_now_playing_embed


def play_queued(ctx):
	music_player = ctx["music_player"]
	music_player.play()


def play_url_now(ctx):
	music_player = ctx["music_player"]
	url = ctx["command_args"][0]

	music_player.enqueue(url)
	music_player.play()


class Play(Command):
	async def run(self, ctx):
		music_player = ctx["music_player"]
		url = ctx["command_args"]
		message = ctx["message"]

		if music_player is None:
			print("No music player instance found!")
			return

		music_player.stop()
		if len(url) == 0:
			play_queued(ctx)
		else:
			play_url_now(ctx)

		song = music_player.now_playing()
		await message.channel.send(embed=new_now_playing_embed(song))
