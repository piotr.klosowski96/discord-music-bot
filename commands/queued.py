from commands.command import Command
from embeds.playlist import new_playlist_embed


class Queued(Command):
	async def run(self, ctx):
		music_player = ctx["music_player"]
		message = ctx["message"]

		if music_player is None:
			print("No music player instance found!")
			return

		playlist = music_player.playlist()[0:10]
		await message.channel.send(embed=new_playlist_embed(playlist))
