from commands.command import Command
from embeds.now_playing import new_now_playing_embed


class Now(Command):
	async def run(self, ctx):
		music_player = ctx["music_player"]
		message = ctx["message"]

		if music_player is None:
			print("No music player instance found!")
			return

		song = music_player.now_playing()
		if song is not None:
			await message.channel.send(embed=new_now_playing_embed(song))
		else:
			await message.channel.send("No song is currently playing!")
