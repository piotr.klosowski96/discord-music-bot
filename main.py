import os
import discord
from typing import AnyStr
from player.player import MusicPlayer
from commands.command import Command
from commands.summon import Summon
from commands.play import Play
from commands.queued import Queued
from commands.queue import Queue
from commands.skip import Skip
from commands.now import Now


class MyClient(discord.Client):
	def __init__(self, **options):

		super().__init__(**options)
		self.ffmpeg_path = options["ffmpeg_path"]
		self.token_path = options["token"]
		self.commands = {}
		self.invoke_char = "!"
		self.music_player = MusicPlayer(self.ffmpeg_path)

		self._register_command("summon", Summon())
		self._register_command("play", Play())
		self._register_command("queued", Queued())
		self._register_command("queue", Queue())
		self._register_command("skip", Skip())
		self._register_command("now", Now())

	def _register_command(self, name: AnyStr, command: Command):
		if name in self.commands:
			raise AssertionError("Command {} is already registered!".format(name))

		self.commands[name] = command

	async def on_ready(self):
		print('We have logged in as {0.user}'.format(client))

	async def on_message(self, message):
		if message.author == client.user:
			return

		if message.content.startswith(self.invoke_char):
			text: AnyStr = str(message.content[1:])
			command = text.split(" ")

			ctx = {
				"command_args": command[1:],
				"message": message,
				"music_player": self.music_player,
			}

			if command[0] in self.commands:
				await self.commands[command[0]].run(ctx)


if __name__ == '__main__':
	token = os.getenv("DISCORD_API_KEY")
	ffmpeg_path = os.getenv("FFMPEG_PATH")

	if token == "":
		print("No discord api token provided!")

	client = MyClient(token=token, ffmpeg_path=ffmpeg_path)
	client.run(token)
